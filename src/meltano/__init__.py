"""Meltano."""
# flake8: noqa  # ignore WPS410, WPS412

# Managed by bumpversion
__version__ = "1.94.0"
